# README #

Simple Python3 module for running safe experiments. The module wraps the Python open() call for opening files, so that files are automatically created in a unique subdirectory together with source code and system call used to generate them. 

### Installation ###
Install by running the `install.sh` script.

### Usage ###

```python
import safe_experiments as se

sf = se.SafeFiles('test')

f = sf.open('exp')
for i in range(1000):
    f.write('i')
f.write('\n')

g = sf.open('exp2')
g.write('asdfsadfsf\n')
```