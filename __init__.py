# Package for running safe experiments where the source code
# and command line code is saved together with the output.

import sys
import os
import datetime

class SafeFiles:
    """
    Class for ensuring that relevant meta-data is attached to the
    data file.

    Initialize with a dirname, and open any number of extra data
    files in the directory with open().
    """

    def __init__(self, dirname):
        """
        Initialize with a name of a folder to put data files in.
        A unique time-date stamp will be appended to the dir name,
        containing a file 'meta' with system call and source code.
        """
        time = datetime.datetime.now().isoformat()
        self.dirname = dirname + time
        os.mkdir(self.dirname)
        meta = open(self.dirname+'/meta', 'w')
        meta.write('# call: ' + str(sys.argv) + '\n\n')
        meta.write('# source code:\n\n')
        if len(sys.argv) > 0:
            source = open(sys.argv[0])
            meta.write(source.read())
        else:
            print('warning: no system call info')
            meta.write('no source code')
        meta.close()
    
    def open(self, filename, **kvargs):
        """Open a file to put experimental data in. 
        Works as standard open() function, except for different defaults 
        and file automatically created in SafeFile subdirectory.

        The new defaults are:
        - the file is opened in Exclusive mode x
        - the buffering size is set 1 (so output immediately visisible)

        Override by the standard open() parameters.
        """
        filename = filename or 'data'
        kvargs['mode'] = kvargs['mode'] if 'mode' in kvargs else 'x'
        kvargs['buffering'] = kvargs['buffering'] if 'buffering' in kvargs else 1
        print(kvargs)
        try:
            f = open(self.dirname + '/' + filename, **kvargs)
        except(FileExistsError):
            print("The filename "+filename+"already exists. \
            Please specify another filename.")
        return f

##################################
## Example use
##################################

# import safe_experiments as se

# sf = se.SafeFiles('test')

# f = sf.open('exp')
# for i in range(1000):
#     f.write('i')
# f.write('\n')

# g = sf.open('exp2')
# g.write('asdfsadfsf\n')
